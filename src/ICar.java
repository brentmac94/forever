/**
 * Different types in Java:
 * - class
 * - enum
 * - primative
 * - interface
 * 
 * A class is a combination of data and methods.
 * 
 * An interface only defines methods that its subclasses will have
 * By definition, everything in an interface is public
 * 
 * @author BMac
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
