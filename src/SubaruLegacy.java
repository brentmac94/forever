/**
 * 
 * @author BMac
 *
 */

public class SubaruLegacy extends ACar {
	
	public SubaruLegacy(int inYear, int inMileage) 
	{
		super("Subaru", "Legacy", 2014);
		setMileage(25000);
	}

}
