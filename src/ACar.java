/**
 * Common workFlow in Java:
 * 
 * Create an interface (let's us program in general)
 * Create an abstract class (lets us code shared functionallity
 * Create a concrete class
 * 
 * Abstract:
 * - Cannot be instantitated
 * - Concrete classes that inherits from the abstract class "must" implement all the abstract methods
 * - Abstract subclasses may or may not implement the abstract methods
 *
 * implments means I'm inheririting from an interface
 * You can implement as many interfaces as you want
 *
 * Everything extends object
 *
 * @author bmac
 *
 */

public abstract class ACar implements ICar{
	
	/** The mqke of the car */
	private String make;
	
	/** model of the car */
	private String model;
	
	/** The year of the car */
	private int year;
	
	/** milage of the car */
	private int mileage;
	
	/** constructor for abstract car
	 * @param inMake
	 * @param inModel
	 * @param inYear */
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	 @Override
	public String toString() {
		// TODO Auto-generated method stub
		return getMake () + " " + getModel();
	}
	
	
}
